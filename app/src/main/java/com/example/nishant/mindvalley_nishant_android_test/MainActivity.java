package com.example.nishant.mindvalley_nishant_android_test;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.nishant.mindvalley_nishant_android_test.adapter.DashBoardAdapter;
import com.example.nishant.mindvalley_nishant_android_test.fragments.DashBoardFragment;
import com.example.nishant.mindvalley_nishant_android_test.fragments.UserScreenFragment;
import com.example.nishant.mindvalley_nishant_android_test.models.MasterResponse;
import com.example.nishant.mindvalley_nishant_android_test.models.User;
import com.example.nishant.mindvalley_nishant_android_test.network.MVNetworkClient;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements DashBoardAdapter.ILoadUserDetailListener, View.OnClickListener {

    private String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.fbMain)
    public FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            addFragment(R.id.fragmentContainer, new DashBoardFragment(), TAG);
        }

        floatingActionButton.setOnClickListener(this);
    }

    @Override
    public void loadUser(User user) {
        Intent intent = new Intent(this, UserDetailActivity.class);
        intent.putExtra(UserDetailActivity.PROFILE_URL, user.getProfileImage().getLarge());
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, IntensiveImageTest.class);
        startActivity(intent);
    }
}
