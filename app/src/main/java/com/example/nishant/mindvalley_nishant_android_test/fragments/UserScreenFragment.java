package com.example.nishant.mindvalley_nishant_android_test.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.nishant.mindvalley_nishant_android_test.R;
import com.example.nishant.mindvalley_nishant_android_test.models.User;
import com.example.nishant.mindvalley_nishant_android_test.network.MVNetworkClient;
import com.example.nishant.mindvalley_nishant_android_test.network.ResponseListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nishant on 12/4/2016.
 */

public class UserScreenFragment extends BaseFragment {
    private static final String PROFILE_URL = "profile_url";

    public static UserScreenFragment getFragment(String user) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(PROFILE_URL, user);
        UserScreenFragment userScreenFragment = new UserScreenFragment();
        userScreenFragment.setArguments(bundle);
        return userScreenFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_detail, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

}
