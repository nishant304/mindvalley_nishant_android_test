package com.example.nishant.mindvalley_nishant_android_test;

import java.util.List;

/**
 * Created by nishant on 12/4/2016.
 */

public class JsonTestClass {

    public Integer id;
    public String name;
    public String price;
    public List<String> tags;

}
